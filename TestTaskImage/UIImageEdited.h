//
//  UIImageEdited.h
//  TestTaskImage
//
//  Created by Admin on 09.03.17.
//  Copyright © 2017 Polli. All rights reserved.
//

typedef NS_ENUM(NSInteger, UIImageEdite) {
    UIImageEditedRotate,
    UIImageEditedBlackWhite,
    UIImageEditedInvert,
    UIImageEditedMirror,
};

