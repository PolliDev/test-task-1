//
//  UIImage+Filters.h
//  TestTaskImage
//
//  Created by Admin on 08.03.17.
//  Copyright © 2017 Polli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Filters)
+ (UIImage *)imageByRotatingImage:(UIImage *)originalImage fromImageOrientation:(UIImageOrientation)orientation;
+ (UIImage *)imageByInvertImage:(UIImage *)originalImage;
+ (UIImage *)imageByBlackWhiteImage:(UIImage *)originalImage;
@end
