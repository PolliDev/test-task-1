//
//  ViewController.m
//  TestTaskImage
//
//  Created by Admin on 07.03.17.
//  Copyright © 2017 Polli. All rights reserved.
//

#import "ViewController.h"
#import "ImageStorage.h"

static NSString *const kImageCellIdentifier = @"ImageCellIdentifier";

@interface ViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *imageTableView;
@property (weak, nonatomic) IBOutlet UIImageView *originalImageView;
@property (nonatomic) UIImagePickerController *picker;
@property (nonatomic) ImageStorage *imageSrorage;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imageSrorage = [[ImageStorage alloc]init];
    [self.imageTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kImageCellIdentifier];
    [self.imageTableView reloadSections:[NSIndexSet indexSetWithIndex:0]
                  withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - Actions

- (IBAction)tapOriginalImage:(id)sender
{
    self.picker = [[UIImagePickerController alloc]init];
    self.picker.delegate = self;
    [self.picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:self.picker animated:YES completion:nil];
}
- (IBAction)rotateImageButton:(id)sender
{
    [self.imageSrorage addImagesObject:UIImageEditedRotate];
    [self.imageTableView reloadData];
}
- (IBAction)blackWhiteButton:(id)sender
{
    [self.imageSrorage addImagesObject:UIImageEditedBlackWhite];
    [self.imageTableView reloadData];
}
- (IBAction)mirrorImage:(id)sender
{
    [self.imageSrorage addImagesObject:UIImageEditedMirror];
    [self.imageTableView reloadData];
}
- (IBAction)invertButton:(id)sender
{
    [self.imageSrorage addImagesObject:UIImageEditedInvert];
    [self.imageTableView reloadData];
}


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    self.imageSrorage.originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.originalImageView setImage:self.imageSrorage.originalImage];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.imageSrorage.images.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kImageCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kImageCellIdentifier];
    }
    NSLog(@"%li", indexPath.row);
    UIImage *i = [self.imageSrorage.images objectAtIndex:indexPath.row];
    cell.imageView.image = i;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"Save"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                             UIImageWriteToSavedPhotosAlbum([self.imageSrorage.images objectAtIndex:indexPath.row], nil, nil, nil);
                                                             [self alertSave];
                                                         }];
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Delete"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                               [self.imageSrorage.images removeObjectAtIndex:indexPath.row];
                                                               [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                                                                withRowAnimation:UITableViewRowAnimationFade];
                                                           }];
    UIAlertAction *addFilterAction = [UIAlertAction actionWithTitle:@"Add filter"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                               self.imageSrorage.originalImage = [self.imageSrorage.images objectAtIndex:indexPath.row];
                                                               [self.originalImageView setImage:self.imageSrorage.originalImage];
                                                           }];
    [alertController addAction:addFilterAction];
    [alertController addAction:saveAction];
    [alertController addAction:cancelAction];
    [alertController addAction:deleteAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}


#pragma mark - Alert


- (void)alertSave
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Photo was save"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
