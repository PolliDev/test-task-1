//
//  EditableImage.h
//  TestTaskImage
//
//  Created by Admin on 09.03.17.
//  Copyright © 2017 Polli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImageEdited.h"

@protocol EditableImage <NSObject>
- (void)addImagesObject:(UIImageEdite)typeEdited;
@end
