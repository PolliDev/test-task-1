//
//  ImageStorage.h
//  TestTaskImage
//
//  Created by Admin on 09.03.17.
//  Copyright © 2017 Polli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "EditableImage.h"

@interface ImageStorage : NSObject <EditableImage>

@property (nonatomic, readwrite) UIImage *originalImage;
@property (nonatomic, readwrite) UIImage *editedImage;
@property (nonatomic, readwrite) NSMutableArray *images;


@end
