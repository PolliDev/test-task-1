//
//  ImageStorage.m
//  TestTaskImage
//
//  Created by Admin on 09.03.17.
//  Copyright © 2017 Polli. All rights reserved.
//

#import "ImageStorage.h"
#import "UIImage+Filters.h"

@implementation ImageStorage



- (instancetype)init
{
    self = [super init];
    if (self) {
        _images = [NSMutableArray array];
        _originalImage = [[UIImage alloc]init];
        _editedImage = [[UIImage alloc]init];
    }
    
    return self;
}


- (void)addImagesObject:(UIImageEdite)typeEdited
{
    switch (typeEdited) {
        case UIImageEditedInvert:
            self.editedImage = [UIImage imageByInvertImage:self.originalImage];
            [self.images addObject:self.editedImage];
            break;
        case UIImageEditedMirror:
            self.editedImage = [UIImage imageByRotatingImage:self.originalImage fromImageOrientation:UIImageOrientationDownMirrored];
            [self.images addObject:self.editedImage];
            break;
        case UIImageEditedRotate:
            self.editedImage = [UIImage imageByRotatingImage:self.originalImage fromImageOrientation:UIImageOrientationRight];
            [self.images addObject:self.editedImage];
            break;
        case UIImageEditedBlackWhite:
            self.editedImage = [UIImage imageByBlackWhiteImage:self.originalImage];
            [self.images addObject:self.editedImage];
            break;
    }
}
@end
